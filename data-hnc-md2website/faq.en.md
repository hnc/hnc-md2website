# ![imgtxt]({img_dir}/oxygen-icons/64x64/apps/help-browser.png)FAQ - Frequently Asked Questions

*What is `hnc-md2website`?*

`hnc-md2website` is a website generator from
![imgtxt]({img_dir}/logo/markdown-mark.64x64.png)[Markdown](https://en.wikipedia.org/wiki/Markdown) files.
