#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2021 Lénaïc Bagnères, lenaicb@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# index

data = {}

data["website_title"] = {"en": "hnc-md2website", "fr": "hnc-md2website"}

data["favicon_path"] = {
    "en": "{img_dir}/oxygen-icons/64x64/apps/konqueror.png",
    "fr": "{img_dir}/oxygen-icons/64x64/apps/konqueror.png",
}

# header

data["header_left"] = {
    "en": '<img src="{img_dir}/oxygen-icons/64x64/apps/konqueror.png" alt="icon" class="imgtxt"><a href="https://bagneres.org/hnc-md2website/"><b><code>hnc-md2website</code></b></a> <br>\n'
    + "<i>Generate you website from markdown and python files</i> <br>\n"
    + 'Free software on <img src="{img_dir}/logo/gitlab.64x64.png" alt="icon" class="imgtxt"><a href="https://gitlab.com/hnc/hnc-md2website"><code>gitlab.com/hnc/hnc-md2website</code></a>',
    "fr": '<img src="{img_dir}/oxygen-icons/64x64/apps/konqueror.png" alt="icon" class="imgtxt"><a href="https://bagneres.org/hnc-md2website/"><b><code>hnc-md2website</code></b></a> <br>\n'
    + "<i>Génère votre site web depuis du markdown et du python</i> <br>"
    + 'Logiciel libre sur <img src="{img_dir}/logo/gitlab.64x64.png" alt="icon" class="imgtxt"><a href="https://gitlab.com/hnc/hnc-md2website"><code>gitlab.com/hnc/hnc-md2website</code></a>',
}

data["header_right"] = {
    "en": '<img src="{img_dir}/oxygen-icons/64x64/apps/konqueror.png" alt="Logo">',
    "fr": '<img src="{img_dir}/oxygen-icons/64x64/apps/konqueror.png" alt="Logo">',
}

# menu

data["menu"] = {
    "en": '<img src="{img_dir}/oxygen-icons/64x64/apps/knewsticker.png" alt="icon" class="imgtxt"><a href="{page_index}"><code>hnc-md2website</code></a> '
    + '<img src="{img_dir}/oxygen-icons/64x64/categories/preferences-system.png" alt="icon" class="imgtxt"><a href="{page_dir}how-to{page_suffix}">How to</a> '
    + '<img src="{img_dir}/oxygen-icons/64x64/apps/help-browser.png" alt="icon" class="imgtxt"><a href="{page_dir}faq{page_suffix}">FAQ</a>',
    "fr": '<img src="{img_dir}/oxygen-icons/64x64/apps/knewsticker.png" alt="icon" class="imgtxt"><a href="{page_index}"><code>hnc-md2website</code></a> '
    + '<img src="{img_dir}/oxygen-icons/64x64/categories/preferences-system.png" alt="icon" class="imgtxt"><a href="{page_dir}how-to{page_suffix}"><i>How to</i></a> '
    + '<img src="{img_dir}/oxygen-icons/64x64/apps/help-browser.png" alt="icon" class="imgtxt"><a href="{page_dir}faq{page_suffix}">FAQ</a>',
}

# footer

data["footer"] = {
    "en": 'Most used icons come from <img src="{img_dir}/oxygen-icons/64x64/apps/oxygen.png" alt="icon" class="imgtxt"><a href="https://github.com/KDE/oxygen-icons">Oxygen</a> |\n'
    + 'Flags come from <img src="{img_dir}/flag-icons/flags/1x1/un.svg" alt="icon" class="imgtxt"><a href="https://github.com/lipis/flag-icons">flag-icons</a> | Some logos are also used <br>\n'
    + 'Powered by <img src="{img_dir}/oxygen-icons/64x64/apps/konqueror.png" alt="icon" class="imgtxt"><a href="https://bagneres.org/hnc-md2website/"><code>hnc-md2website</code></a> | Source code on <img src="{img_dir}/logo/gitlab.64x64.png" alt="icon" class="imgtxt"><a href="https://gitlab.com/hnc/hnc-md2website"><code>gitlab.com/hnc/hnc-md2website</code></a> <br>\n'
    + 'Copyright © 2021 Lénaïc <span class="small_caps">Bagnères</span>, lenaicb@singularity.fr <br>',
    "fr": 'La plupart des icônes sont de <img src="{img_dir}/oxygen-icons/64x64/apps/oxygen.png" alt="icon" class="imgtxt"><a href="https://github.com/KDE/oxygen-icons">Oxygen</a> |\n'
    + 'Les drapaux sont de <img src="{img_dir}/flag-icons/flags/1x1/un.svg" alt="icon" class="imgtxt"><a href="https://github.com/lipis/flag-icons">flag-icons</a> | Plusieurs logos sont aussi utilisés <br>\n'
    + 'Propulsé par <img src="{img_dir}/oxygen-icons/64x64/apps/konqueror.png" alt="icon" class="imgtxt"><a href="https://bagneres.org/hnc-md2website/"><code>hnc-md2website</code></a> | Source code sur <img src="{img_dir}/logo/gitlab.64x64.png" alt="icon" class="imgtxt"><a href="https://gitlab.com/hnc/hnc-md2website"><code>gitlab.com/hnc/hnc-md2website</code></a> <br>\n'
    + 'Copyright © 2021 Lénaïc <span class="small_caps">Bagnères</span>, lenaicb@singularity.fr <br>',
}
