#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2021-2024 Lénaïc Bagnères, lenaicb@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import marko
import os
import shutil
import subprocess
import sys

os.chdir(os.path.dirname(os.path.realpath(__file__)))
sys.path.append("../hnc-python3/src")
import hnc.term

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage:", sys.argv[0], " <data_path>")
        sys.exit(1)

    print()

    # --------------------------------------------------------------------------
    # Command line arguments
    # --------------------------------------------------------------------------

    print(hnc.term.blue("COMMAND LINE ARGUMENTS"))
    print()

    data_path = sys.argv[1]
    script_path = os.path.dirname(os.path.realpath(__file__))
    output_dir = os.path.join("build", data_path[data_path.rfind("/") + 6 :])

    print("Output directory =", output_dir)
    print()

    # --------------------------------------------------------------------------
    # Index (header, menu, footer)
    # --------------------------------------------------------------------------

    print(hnc.term.blue("READ INDEX"))
    print()

    with open(os.path.join(script_path, "html", "index.raw.html"), "r") as f:
        index_raw = f.read()

    sys.path.append(data_path)
    import data as data_module

    # Swap keys and langs
    tmp = data_module.data
    data = {}
    keys = tmp.keys()
    langs = set()
    for key in tmp:
        langs.update(set(tmp[key].keys()))
    for lang in langs:
        if lang not in data:
            data[lang] = {}
        for key in keys:
            data[lang][key] = tmp[key][lang]
        if "body_max_width" not in keys:
            data[lang]["body_max_width"] = "800px"

    header_lang = ""
    for lang in sorted(langs):
        header_lang += '<a href="../{page_dir}' + lang + '{page_suffix}">'
        header_lang += (
            '<img src="{img_dir}/flag-icons/flags/1x1/'
            + ("us" if lang == "en" else lang)
            + '.svg" alt="icon" class="imgtxt"><small><code>'
            + lang.upper()
            + "</code></small>"
        )
        header_lang += "</a><br> "

    index = {}
    for lang in langs:
        index[lang] = index_raw.format(
            **data[lang],
            header_lang=header_lang,
            article="{article}",
            css_path="{css_path}",
            lang=lang,
            page_dir="{page_dir}",
            page_suffix="{page_suffix}",
            page_index="{page_index}",
            img_dir="{img_dir}",
            page_ext="html"
        )

    # --------------------------------------------------------------------------
    # Output Directories, Images, CSS and Index
    # --------------------------------------------------------------------------

    print(hnc.term.blue("CREATE OUTPUT DIRECTORIES, COPY IMAGES, ..."))
    print()

    # Images

    if os.path.exists(os.path.join(output_dir, "img")) == False:
        shutil.copytree(
            os.path.join(script_path, "img"), os.path.join(output_dir, "img")
        )

    if os.path.exists(os.path.join(output_dir, "img", "flag-icons")) == False:
        shutil.copytree(
            os.path.join(script_path, "..", "flag-icons"),
            os.path.join(output_dir, "img", "flag-icons"),
        )

    if os.path.exists(os.path.join(output_dir, "img", "oxygen-icons")) == False:
        shutil.copytree(
            os.path.join(script_path, "..", "oxygen-icons"),
            os.path.join(output_dir, "img", "oxygen-icons"),
        )

    # Output directories + CSS

    if os.path.exists(output_dir) == False:
        os.mkdir(output_dir)

    output_dirs = {}
    for lang in langs:
        output_dirs[lang] = os.path.join(output_dir, lang)
        if os.path.exists(output_dirs[lang]) == False:
            os.mkdir(output_dirs[lang])
        shutil.copy(
            os.path.join(script_path, "html", "css.css"),
            os.path.join(output_dirs[lang], "css.css"),
        )

    # index.html

    shutil.copy(os.path.join(script_path, "html", "index.html"), output_dir)

    # --------------------------------------------------------------------------
    # Pages
    # --------------------------------------------------------------------------

    print(hnc.term.blue("GENERATE PAGES"))
    print()

    in_subdir = True

    def format_page_part(page_part, page_in_subdir, page_is_index):
        # Page is the index
        if page_is_index:
            # Page is in a subdirecty: lang/page_name/index.{page_ext}
            if page_in_subdir:
                # return page_part.format(page_ext='html', page_dir='./', page_suffix='/index.html', page_index='index.html', img_dir='../img', css_path='css.css', article='{article}')
                return page_part.format(
                    page_ext="html",
                    page_dir="./",
                    page_suffix="/",
                    page_index="",
                    img_dir="../img",
                    data_dir="../data",
                    css_path="css.css",
                    article="{article}",
                )
            # Page is not in a subdirecty: lang/page_name.{page_ext}
            else:
                return page_part.format(
                    page_ext="html",
                    page_dir="./",
                    page_suffix=".html",
                    page_index="index.html",
                    img_dir="../img",
                    data_dir="../data",
                    css_path="css.css",
                    article="{article}",
                )
        # Page is not the index
        else:
            # Page is in a subdirecty: lang/page_name/index.{page_ext}
            if page_in_subdir:
                # return page_part.format(page_ext='html', page_dir='../', page_suffix='/index.html', page_index='index.html', img_dir='../../img', css_path='../css.css', article='{article}')
                return page_part.format(
                    page_ext="html",
                    page_dir="../",
                    page_suffix="/",
                    page_index="../",
                    img_dir="../../img",
                    data_dir="../../data",
                    css_path="../css.css",
                    article="{article}",
                )
            # Page is not in a subdirecty: lang/page_name.{page_ext}
            else:
                return page_part.format(
                    page_ext="html",
                    page_dir="./",
                    page_suffix=".html",
                    page_index="index.html",
                    img_dir="../img",
                    data_dir="../../data",
                    css_path="css.css",
                    article="{article}",
                )

    article_filenames = {}
    for lang in langs:
        article_filenames[lang] = []
    for article_filename in sorted(os.listdir(data_path)):
        for lang in langs:
            if article_filename.endswith("." + lang + ".md"):
                article_filenames[lang] += [article_filename]

    for lang in langs:

        for article_filename in article_filenames[lang]:

            article_path = os.path.join(data_path, article_filename)

            page_in_subdir = in_subdir
            page_is_index = article_filename == "index." + lang + ".md"

            with open(article_path, "r") as f:
                article_markdown = f.read()
                article_markdown = format_page_part(
                    article_markdown, page_in_subdir, page_is_index
                )
                article_html = marko.convert(article_markdown)

            # Insert article
            page = format_page_part(index[lang], page_in_subdir, page_is_index)
            page = page.format(article=article_html)
            # Hacks
            page = page.replace('alt="imgtxt"', 'alt="icon" class="imgtxt"')

            # Page is not in a subdirecty: lang/page_name.{page_ext}
            if page_in_subdir == False or page_is_index:
                page_basename = article_filename[0 : -len("." + lang + ".md")] + ".html"
                with open(os.path.join(output_dirs[lang], page_basename), "w") as f:
                    f.write(page)
            # Page is in a subdirecty: lang/page_name/index.{page_ext}
            else:
                page_dir = article_filename[0 : -len("." + lang + ".md")]
                page_basename = "index.html"
                if os.path.exists(os.path.join(output_dirs[lang], page_dir)) == False:
                    os.mkdir(os.path.join(output_dirs[lang], page_dir))
                with open(
                    os.path.join(output_dirs[lang], page_dir, page_basename), "w"
                ) as f:
                    f.write(page)

    # --------------------------------------------------------------------------
    # Directories
    # --------------------------------------------------------------------------

    for filename in sorted(os.listdir(data_path)):
        if filename != "__pycache__":
            path = os.path.join(data_path, filename)
            if os.path.isdir(path):
                if os.path.exists(os.path.join(output_dir, filename)) == False:
                    shutil.copytree(
                        path, os.path.join(output_dir, filename), symlinks=True
                    )

    # --------------------------------------------------------------------------
    # Check
    # --------------------------------------------------------------------------

    print(hnc.term.blue("CHECK FILES WITH https://validator.w3.org"))
    print()

    def check_html(directory):
        for filename in sorted(os.listdir(directory)):
            path = os.path.join(directory, filename)
            if os.path.isdir(path):
                check_html(path)
            elif path.endswith(".html"):
                print(hnc.term.blue("File " + path))
                cmd = [
                    "curl",
                    "-H",
                    "Content-Type: text/html; charset=utf-8",
                    "--data-binary",
                    "@" + path,
                    "https://validator.w3.org/nu/?out=json",
                ]
                p = subprocess.Popen(
                    cmd,
                    stdin=subprocess.PIPE,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                p_output, p_err = p.communicate()
                p_return_code = p.returncode
                if p_return_code == 0:
                    try:
                        out_json = json.loads(p_output)
                    except Exception as e:
                        print(hnc.term.red("ERROR: json conversion failed"))
                        print("p_output =")
                        print(p_output)
                        print("p_err =")
                        print(p_err)
                        print()
                    if len(out_json["messages"]) == 0:
                        print(hnc.term.green("Ok"))
                    print()
                    for message in out_json["messages"]:
                        if message["type"] == "error":
                            print(hnc.term.red("Error"))
                        elif message["type"] == "info":
                            print(hnc.term.blue("Info"))
                        print(message)
                        print()
                else:
                    print(hnc.term.red("ERROR: curl failed"))
                    print("cmd =")
                    print(cmd)
                    print("p_output =")
                    print(p_output)
                    print("p_err =")
                    print(p_err)
                    print()

    # check_html(output_dir)

    # --------------------------------------------------------------------------
    # End
    # --------------------------------------------------------------------------

    sys.exit(0)
