# `hnc-md2website`

`hnc-md2website` generates static web site from markdown files.

The [website of `hnc-md2website`](https://bagneres.org/hnc-md2website/) is
generated with `hnc-md2website`.

## License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Dependencies

Python 3 with `marko`.

You can install `marko` with `pip3`
```sh
pip3 install marko --break-system-packages
```

[Oxygen icons](https://github.com/KDE/oxygen-icons) and
[flag-icons](https://github.com/lipis/flag-icons).
Clone both repositories in the parent directory of `hnc-md2website`.
```sh
git clone https://github.com/KDE/oxygen-icons.git
git clone https://github.com/lipis/flag-icons.git
```

## Usage

```sh
python3 hnc-md2website.py <data_path>
```

## W3C Validator

To validate `HTML`, we use <https://validator.w3.org> by default.

On Debian GNU/Linux, it is possible to install this package (with root access):
```sh
apt install w3c-markup-validator
```
The local address of the validator is <http://localhost/w3c-validator/>.
